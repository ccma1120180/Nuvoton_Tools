# Development Tool
[Nu-Link driver and NuTool](https://www.nuvoton.com/hq/support/tool-and-software/software/development-tool/?__locale=en)
# Programmer
[ICPTool and ISPTool](https://www.nuvoton.com/hq/support/tool-and-software/software/programmer/?__locale=en)
# Nu-Link2 open platform
## Open source firmware
[DAPLink on Nu-Link2](https://github.com/OpenNuvoton/DapLink)   
[ISP bridge](https://github.com/OpenNuvoton/NuLink2_ISP_Bridge)    
[ICP library](https://github.com/OpenNuvoton/NuLink2_ICP_Library)
## On chip debugging
[pyOCD for Nu-Link2 (using CMSIS-DAP)](https://github.com/OpenNuvoton/pyOCD)
